import gi
gi.require_version("Gtk", "3.0")

from gi.repository import Gtk

#Definir la clase de la ventana principal 
class window():
    
    def __init__(self):            
        #Comandos para abrir ventana    
        self.builder = Gtk.Builder()
        self.builder.add_from_file("ventana.glade")
        self.window = self.builder.get_object("window")
        self.window.set_default_size(800, 600)
        self.window.connect("destroy", Gtk.main_quit)
        #extraer los textos ingresados
        self.primerTexto = self.builder.get_object("primerTexto")
        self.segundoTexto = self.builder.get_object("segundoTexto")
        self.suma = self.builder.get_object("suma")
        #dar funcion a los botones
        self.button_accept = self.builder.get_object("accept")
        self.button_reset = self.builder.get_object("reset")
 
        for event in ["activate", "changed"]:
            self.primerTexto.connect(event, self.update)
            self.segundoTexto.connect(event, self.update)
        #Pasarle una funcion a los botones 
        self.button_accept.connect("clicked", self.infoSuma)
        self.button_reset.connect("clicked", self.confirm_reset)
        #mostrar la ventana
        self.window.show_all()
        
    def update(self,btn=None):
        primerTexto = self.primerTexto.get_text()
        segundoTexto = self.segundoTexto.get_text()

        self.suma.set_value(len(primerTexto) + len(segundoTexto))
        print(VALORSUMA)
    #funcion de sumar las letras ingresadas con un valor numerico 
    def infoSuma(self, btn=None):
        primerTexto = self.primerTexto.get_text()
        segundoTexto = self.segundoTexto.get_text()
        VALORSUMA = len(primerTexto) + len(segundoTexto)
        x =Ventana2(primerTexto, segundoTexto, VALORSUMA)
        x.window2.run()
        x.primetTexto.set_text("sumatextos")
        x.segundoTexto.set_text("sumatextos")
     #funcion del boton cancelar que muestra una ventana de advertencia 
    def confirm_reset(self, btn=None):
        dialog = Gtk.MessageDialog(type=Gtk.MessageType.WARNING, buttons=Gtk.ButtonsType.YES_NO)
        dialog.format_secondary_text("Se eliminaran los datos anteriores")

        def response(dialogo, response):
            dialog.close()
        
            if response == Gtk.ResponseType.YES:
                self.reset()

        dialog.connect("response", response)
        dialog.run()

    def reset(self,btn=None):
        self.primerTexto.set_text("")
        self.segundoTexto.set_text("")
        self.update()
        
#Definir la clase de la ventana con los resultados        
class Ventana2():
	
    def __init__(self, primerTexto, segundoTexto, VALORSUMA):
        #parametros asignados a una variable
        self.primerTexto = primerTexto
        self.segundoTexto = segundoTexto
        self.X = VALORSUMA
        #suma de textos
        length = len(primerTexto) + len(segundoTexto)
        text = "Texto 1:\n" + primerTexto
        text += "\n\nTexto 2:\n" + segundoTexto
        text += "\n\nLargo de ambas cadenas: " + str(length)
        print(length)
        #construccion de la nueva ventana
        self.builder = Gtk.Builder()
        self.builder.add_from_file("ventana.glade")
        self.window2 = self.builder.get_object("VentanaAceptar")
        self.window2.set_default_size(400, 300)
        self.window2.connect("destroy", Gtk.main_quit)
        
        #definimos la funcion de los botones 
        self.button_aceptar2 = self.builder.get_object("aceptar2")
        self.button_aceptar2.connect("clicked", self.boton_aceptar2) 
        self.respuesta = self.builder.get_object("sumatextos")
        self.respuesta2 = self.builder.get_object("valorSuma")
        
        suma = self.primerTexto + self.segundoTexto 
        self.respuesta.set_text(suma)      
        self.respuesta2.set_value(VALORSUMA)
                   
        
        #se muestra la ventana 2
        self.window2.show_all()
    #al apretar aceptar se cierra todo     
    def boton_aceptar2(self, event):
        self.window2.destroy()
#main
if __name__ == "__main__":
    w = window()
    Gtk.main()
